package gnu.gettext;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Gettext {

	public Gettext() {
	}

	public Gettext(Map map) {
		this.map.putAll(map);
	}

	private Map<String, String> map = new HashMap<String, String>();

	public String _(String key) {
		String value = map.get(key);
		if (value == null) {
			System.out.println("!key = '" + key + "'");
			return key.toUpperCase();
		}
		//System.out.println(key + "=" + value);
		return value;
	}

	public void add(Map<String, String> map) {
		this.map.putAll(map);
	}

	public Map<String, String> allTranslations() {
		return Collections.unmodifiableMap(map);
	}
}
