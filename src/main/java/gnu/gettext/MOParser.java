package gnu.gettext;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class MOParser {

	private int magicNumber;
	private int fileFormatRevision;
	private int numberOfStrings;
	private int offsetOriginal;
	private int offsetTranslation;
	private int sizeOfHash;
	private int offsetHash;

	private Map<String, String> map = new HashMap<String, String>();
	private File moFile;

	public Map<String, String> getMap() {
		return map;
	}

	public void parse(String fileName) {
		//new File("/home/uli/usr/build15/locale/de/LC_MESSAGES/tribe_barbarians.mo");
		moFile = new File(fileName);
		doStuff();
	}

	/**
     *
     */
	private void doStuff() {
		try {
			BufferedInputStream in = new BufferedInputStream(new FileInputStream(moFile));
			//BufferedReader in = new BufferedReader(new FileReader(moFile));
			magicNumber = readInt(in, "Magic");
			fileFormatRevision = readInt(in, "FFR");
			numberOfStrings = readInt(in, "nString");
			offsetOriginal = readInt(in, "Off O");
			offsetTranslation = readInt(in, "Off T");
			sizeOfHash = readInt(in, "nHash");
			offsetHash = readInt(in, "Off H");
			// skip to end of hash (H +S * 4)  minus current position
			int skip = offsetHash + (sizeOfHash * 4) - 28; //???
			byte[] sb = new byte[skip];
			in.read(sb);
			// read strings
			readStrings(in);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @param in
	 * @throws IOException
	 */
	private void readStrings(final BufferedInputStream in) throws IOException {
		List<String> all = new ArrayList<String>();
		int b = -1;
		StringBuilder sb = new StringBuilder();
		do {
			while ((b = in.read()) > 0) {
				//System.out.println(i);
				sb.append((char) b);
			}
			//System.out.println(sb);
			all.add(new String(sb.toString().getBytes("ISO-8859-1")));
			sb.setLength(0);
		} while (b >= 0);
		all.remove(0);// first is always empty
		int strings = all.size() / 2; //must mathc nString!
		for (int i = 0; i < strings; i++) {
			map.put(all.get(i), all.get(i + strings));
			// special 
			if (all.get(i).startsWith("Project-Id-Version")) {
				map.put(" INTERNAL ", all.get(i + strings));
			}
			//System.out.println("ID =" + all.get(i));
			//System.out.println("STR=" + map.get(all.get(i)));
		}
	}

	/**
	 * @param in
	 * @param string
	 * @return
	 * @throws IOException
	 */
	private int readInt(final BufferedInputStream in, final String string) throws IOException {
		int r = readInt(in);
		System.out.println(string + ": " + r);
		return r;
	}

	/**
	 * @param in
	 * @return
	 * @throws IOException
	 */
	private int readInt(final BufferedInputStream in) throws IOException {
		byte[] b = new byte[4];
		in.read(b);
		int r = (b[3] << 24) + ((b[2] & 0xFF) << 16) + ((b[1] & 0xFF) << 8) + (b[0] & 0xFF);
		return r;
	}
}
