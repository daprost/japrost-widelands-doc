package de.japrost.widelands;

import gnu.gettext.Gettext;
import gnu.gettext.MOParser;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.japrost.widelands.tribe.Tribe;

/**
 * Wrapper around one language.
 * 
 * @author <a href="mailto:alexxismachine@alexxismachine.de">Ulrich David</a>
 * @version $Id$
 * @since 1.0
 */
public class I18nReader {

	private String locale;
	private Gettext widelandsMO;
	private Map<String, Gettext> tribeTranslations;
	private File installationDir;

	public I18nReader(File installationDir, String locale) {
		this.locale = locale;
		this.installationDir = installationDir;
	}

	public void doRead(List<Tribe> tribes) {
		// read widelandsMO
		File moFileWL = new File(installationDir, "locale/" + locale + "/LC_MESSAGES/widelands.mo");
		MOParser moWl = new MOParser();
		moWl.parse(moFileWL.getAbsolutePath());
		widelandsMO = new Gettext(moWl.getMap());
		// read tribesMO
		tribeTranslations = new HashMap<String, Gettext>();
		for (Tribe tribe : tribes) {
			File moFile = new File(installationDir, "locale/" + locale + "/LC_MESSAGES/tribe_" + tribe.getCode() + ".mo");
			MOParser mo = new MOParser();
			mo.parse(moFile.getAbsolutePath());
			tribeTranslations.put(tribe.getCode(), new Gettext(mo.getMap()));
		}
	}

	public final Gettext getWidelandsMO() {
		return widelandsMO;
	}

	public final Map<String, Gettext> getTribeTranslations() {
		return tribeTranslations;
	}
}
