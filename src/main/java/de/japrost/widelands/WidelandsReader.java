package de.japrost.widelands;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.japrost.widelands.tribe.Tribe;
import de.japrost.widelands.tribe.TribeReader;

public class WidelandsReader {

	private File baseDir;

	private String tribeDirName = "tribes";

	//	public static void main(String[] args) {
	//		WidelandsReader r = new WidelandsReader();
	//		r.setBaseDir(new File("/home/uli/usr/build15/"));
	//		r.readTribes();
	//	}

	public List<Tribe> readTribes() {
		List<Tribe> result = new ArrayList<Tribe>();
		File[] tribes = new File(baseDir, tribeDirName).listFiles();
		for (File tribeDir : tribes) {
			System.out.println("WR: START reading tribe " + tribeDir);
			TribeReader tr = new TribeReader();
			tr.setBaseDir(tribeDir);
			Tribe tribe = tr.readTribe(tribeDir.getName());
			tribe.setCode(tribeDir.getName());
			//printPreciousThings(tribe);
			result.add(tribe);
			System.out.println("WR: END   reading tribe " + tribeDir);
		}
		return result;
	}

	public void setBaseDir(File baseDir) {
		this.baseDir = baseDir;
	}

	public File getBaseDir() {
		return baseDir;
	}

	/**
	 * @return the tribeDirName
	 */
	public final String getTribeDirName() {
		return tribeDirName;
	}

	/**
	 * @param tribeDirName the tribeDirName to set
	 */
	public final void setTribeDirName(String tribeDirName) {
		this.tribeDirName = tribeDirName;
	}
}
