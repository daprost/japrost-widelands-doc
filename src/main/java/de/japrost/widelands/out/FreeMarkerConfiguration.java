package de.japrost.widelands.out;

import java.util.List;

public class FreeMarkerConfiguration {

	private List<String> locales;
	private List<String> topics;

	public final List<String> getLocales() {
		return locales;
	}

	public final void setLocales(List<String> locales) {
		this.locales = locales;
	}

	public final List<String> getTopics() {
		return topics;
	}

	public final void setTopics(List<String> topics) {
		this.topics = topics;
	}

}
