package de.japrost.widelands.out;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import de.japrost.widelands.I18nReader;
import de.japrost.widelands.WidelandsReader;
import de.japrost.widelands.tribe.Tribe;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import gnu.gettext.Gettext;

public class FreeMarker {

	private final Configuration cfg;
	private File installationDir = new File("/home/uli/usr/build15/");
	private WidelandsReader r = new WidelandsReader();
	private List<Tribe> tribes;
	private Map<String, I18nReader> i18nReaders = new HashMap<String, I18nReader>();

	private FreeMarkerConfiguration freeMarkerConfiguration;

	public FreeMarker() {
		cfg = new Configuration();
		initFreeMarker();
	}

	private void initFreeMarker() {
		cfg.setClassForTemplateLoading(this.getClass(), "/ftl");
		cfg.setObjectWrapper(new DefaultObjectWrapper());
	}

	public static void main(String[] args) throws Exception {
		// what to do
		FreeMarkerConfiguration fc = new FreeMarkerConfiguration();
		List<String> locales = new ArrayList<String>();
		locales.add("de");
		//locales.add("en_GB");
		fc.setLocales(locales);
		List<String> topics = new ArrayList<String>();
		topics.add("Buildings");
		topics.add("Ware types");
		fc.setTopics(topics);

		// do it
		FreeMarker f = new FreeMarker();
		f.setFreeMarkerConfiguration(fc);
		f.readData();
		f.copyStaticResources();
		f.generateMainPage();
		f.generateTribeIntroPages();
		f.generateTribeTopicPages();
		//f.generateBuildingsOverview();
	}

	private void copyStaticResources() {
		try {
			FileUtils.copyDirectory(new File(this.getClass().getResource("/static/").getFile()), new File(r.getBaseDir(), "gdoc/html/"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readData() {
		r.setBaseDir(installationDir);
		tribes = r.readTribes();
		for (String locale : freeMarkerConfiguration.getLocales()) {
			I18nReader i18nReader = new I18nReader(installationDir, locale);
			i18nReader.doRead(tribes);
			i18nReaders.put(locale, i18nReader);
		}
	}

	public void generateMainPage() throws IOException, TemplateException {

		Template temp = cfg.getTemplate("index.ftl");
		File docDir = new File(r.getBaseDir(), "gdoc/html/");
		docDir.mkdirs();
		Map root = new HashMap();
		root.put("tribes", tribes);
		root.put("baseDir", "../../");
		root.put("tribeDirName", r.getTribeDirName());
		root.put("i18n", i18nReaders);
		root.put("fmConfig", freeMarkerConfiguration);
		Writer out = new FileWriter(new File(docDir, "index.html"));
		temp.process(root, out);
		out.flush();
	}

	private void generateTribeIntroPages() throws IOException, TemplateException {
		for (String locale : freeMarkerConfiguration.getLocales()) {
			for (Tribe tribe : tribes) {
				generateTribeIndexPage(locale, tribe);
			}
		}
	}

	private void generateTribeIndexPage(String locale, Tribe tribe) throws IOException, TemplateException {

		Template temp = cfg.getTemplate("locale/index_tribe.ftl");
		File docDir = new File(r.getBaseDir(), "gdoc/html/" + locale + "/");
		docDir.mkdirs();
		Map root = new HashMap();
		root.put("locale", locale);
		root.put("tribes", tribes);
		root.put("tribe", tribe);
		root.put("baseDir", "../../../");
		root.put("tribeDirName", r.getTribeDirName());
		root.put("i18n", i18nReaders);
		root.put("i18nWl", i18nReaders.get(locale).getWidelandsMO());
		root.put("i18nTribe", i18nReaders.get(locale).getTribeTranslations().get(tribe.getCode()));

		root.put("fmConfig", freeMarkerConfiguration);
		Writer out = new FileWriter(new File(docDir, "index_" + tribe.getCode() + ".html"));
		temp.process(root, out);
		out.flush();
	}

	private void generateTribeTopicPages() throws IOException, TemplateException {
		for (String locale : freeMarkerConfiguration.getLocales()) {
			for (Tribe tribe : tribes) {
				for (String topic : freeMarkerConfiguration.getTopics())
					generateTribeTopicPage(locale, tribe, topic);
			}
		}
	}

	private void generateTribeTopicPage(String locale, Tribe tribe, String topic) throws IOException, TemplateException {

		Template temp = cfg.getTemplate("locale/tribe/" + topic + ".ftl");
		// TODO tribe-dir?!
		File docDir = new File(r.getBaseDir(), "gdoc/html/" + locale + "/" + tribe.getCode() + "/");
		docDir.mkdirs();
		Gettext gettext = new Gettext();
		gettext.add(i18nReaders.get(locale).getWidelandsMO().allTranslations());
		gettext.add(i18nReaders.get(locale).getTribeTranslations().get(tribe.getCode()).allTranslations());
		/* Merge data-model with template */
		Map root = new HashMap();
		root.put("tribe", tribe);
		root.put("baseDir", "../../../../");
		root.put("tribeDirName", r.getTribeDirName());
		root.put("i18n", gettext);
		Writer out = new FileWriter(new File(docDir, topic + ".html"));
		temp.process(root, out);
		out.flush();

	}

	public final FreeMarkerConfiguration getFreeMarkerConfiguration() {
		return freeMarkerConfiguration;
	}

	public final void setFreeMarkerConfiguration(FreeMarkerConfiguration freeMarkerConfiguration) {
		this.freeMarkerConfiguration = freeMarkerConfiguration;
	}

}
