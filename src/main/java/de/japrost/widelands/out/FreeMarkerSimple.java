package de.japrost.widelands.out;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;

import de.japrost.widelands.i18n.WidelandsLC;
import de.japrost.widelands.in.ConfSection;
import de.japrost.widelands.in.ListMap;
import de.japrost.widelands.in.WidelandsConf;
import de.japrost.widelands.in.WidelandsEconomy;
import freemarker.ext.beans.BeansWrapper;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateHashModel;

public class FreeMarkerSimple {

	private static final String TOPICS = "topics";
	private static final String TARGETDIR_HTML = "gdoc/html/";
	private static final String LEVEL_LOCALE_TRIBE = "locale_tribe_";
	private static final String LEVEL_LOCALE = "locale_";
	private static final String LEVEL_DEFAULT = "default_";
	private final Configuration cfg;
	private File installationDir = new File("/home/uli/usr/build15/");
	private String templateBase = "/ft2";
	WidelandsConf widelandsConf;
	WidelandsLC widelandsLC;
	WidelandsEconomy widelandsEconomy;
	private ListMap<String, String> templateMap;

	Map root = new HashMap();

	public FreeMarkerSimple() {
		cfg = new Configuration();
		widelandsConf = new WidelandsConf();
		widelandsLC = new WidelandsLC();
		templateMap = new ListMap<String, String>();
		root.put("conf", widelandsConf);
		root.put("LC", widelandsLC);
		initFreeMarker();
	}

	private void initFreeMarker() {
		cfg.setClassForTemplateLoading(this.getClass(), templateBase);
		cfg.setObjectWrapper(new DefaultObjectWrapper());
	}

	public static void main(String[] args) throws Exception {
		// do it
		FreeMarkerSimple f = new FreeMarkerSimple();
		f.readData();
		f.calculateEconomy();
		f.prepareGenerate();
		f.generate();

	}

	/**
	 * Calculate values for economy displaying
	 */
	private void calculateEconomy() {
		widelandsEconomy = new WidelandsEconomy(widelandsConf);
		root.put("eco", widelandsEconomy);
	}

	/**
	 * Collect all the templates.
	 */
	private void prepareGenerate() {
		URL url = this.getClass().getResource(templateBase);
		File templateDir = new File(url.getFile());
		File[] templates = templateDir.listFiles(new FilenameFilter() {

			@Override
			public boolean accept(File dir, String name) {
				// only template files (*.ftl)
				// no template include files (inc_*)
				if (name.endsWith(".ftl") && !name.startsWith("inc_")) {
					return true;
				}
				return false;
			}
		});
		Arrays.sort(templates);
		for (File file : templates) {
			String fileName = file.getName().substring(0, file.getName().length() - 4);
			if (fileName.startsWith(LEVEL_LOCALE_TRIBE)) {
				templateMap.add(LEVEL_LOCALE_TRIBE, fileName.substring(LEVEL_LOCALE_TRIBE.length()));
				if (!fileName.endsWith("-eco")) {
					// "topics" for the menu!
					System.out.println("Adding " + fileName);
					templateMap.add(TOPICS, fileName.substring(LEVEL_LOCALE_TRIBE.length()));
				}
				continue;
			}
			if (fileName.startsWith(LEVEL_LOCALE)) {
				templateMap.add(LEVEL_LOCALE, fileName.substring(LEVEL_LOCALE.length()));
				continue;
			}
			templateMap.add(LEVEL_DEFAULT, fileName);
		}
		root.put(TOPICS, templateMap);

	}

	private void copyStaticResources() {
		try {
			FileUtils.copyDirectory(new File(this.getClass().getResource("/static/").getFile()), new File(installationDir, TARGETDIR_HTML));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private void readData() {
		boolean test = true;
		widelandsConf.setInstallationDir(installationDir);
		if (test) {
			widelandsConf.readTribe(new File(installationDir, "tribes/barbarians"));
		} else {
			widelandsConf.readTribes();
		}
		widelandsLC.setInstallationDir(installationDir);
		if (test) {
			widelandsLC.readLocale("de", widelandsConf);
		} else {
			widelandsLC.readLocales(widelandsConf);
		}
	}

	public void generate() throws IOException, TemplateException {
		BeansWrapper wrapper = BeansWrapper.getDefaultInstance();
		TemplateHashModel staticModels = wrapper.getStaticModels();
		TemplateHashModel toolStatics = (TemplateHashModel) staticModels.get("de.japrost.widelands.out.FreeMarkerTools");
		root.put("tools", toolStatics);
		generateDefault();
		generateLocale();
		generateLocaleTribe();
		copyStaticResources();
	}

	/**
	 * generate for level default
	 * 
	 * @throws IOException
	 * @throws TemplateException
	 */
	public void generateDefault() throws IOException, TemplateException {
		root.put("baseDir", "../../");
		root.put("docDir", "");

		for (String file : templateMap.get(LEVEL_DEFAULT)) {
			Template temp = cfg.getTemplate(file + ".ftl");
			File docDir = new File(installationDir, TARGETDIR_HTML);
			docDir.mkdirs();
			Writer out = new FileWriter(new File(docDir, file + ".html"));
			temp.process(root, out);
			out.flush();
		}
	}

	//	public void generateMainPage() throws IOException, TemplateException {
	//            <td>${subSectionCode}[@displaySectionWithSub conf=subConf sectionCode=subSectionCode/]</td>

	/**
	 * generate for each locale
	 */
	private void generateLocale() throws IOException, TemplateException {
		root.put("baseDir", "../../../");
		root.put("docDir", "../");

		for (String localeCode : widelandsLC.getLocaleCodes()) {
			for (String file : templateMap.get(LEVEL_LOCALE)) {
				System.out.println("Generating Locale for " + localeCode + " from " + file);
				root.put("localeCode", localeCode);

				Template temp = cfg.getTemplate(LEVEL_LOCALE + file + ".ftl");
				File docDir = new File(installationDir, TARGETDIR_HTML + localeCode);
				docDir.mkdirs();
				Writer out = new FileWriter(new File(docDir, file + ".html"));
				temp.process(root, out);
				out.flush();
			}
		}

	}

	/**
	 * generate for each locale and each tribe
	 */
	private void generateLocaleTribe() throws IOException, TemplateException {
		root.put("baseDir", "../../../../");
		root.put("docDir", "../../");

		for (String localeCode : widelandsLC.getLocaleCodes()) {
			root.put("localeCode", localeCode);
			for (String tribeCode : widelandsConf.getTribeCodes()) {
				root.put("tribeCode", tribeCode);
				for (String file : templateMap.get(LEVEL_LOCALE_TRIBE)) {
					if (file.endsWith("-eco")) {
						generateEcoLocaleTribe(localeCode, file.substring(0, file.length() - 4), tribeCode);
					} else {
						System.out.println("Generating LocaleTribe for " + localeCode + " from " + file);
						Template temp = cfg.getTemplate(LEVEL_LOCALE_TRIBE + file + ".ftl");
						File docDir = new File(installationDir, TARGETDIR_HTML + localeCode + "/" + tribeCode);
						docDir.mkdirs();
						Writer out = new FileWriter(new File(docDir, file + ".html"));
						temp.process(root, out);
						out.flush();
					}
				}
			}
		}

	}

	/**
	 * generate wrapper html for dot files for each locale and each tribe
	 */
	private void generateEcoLocaleTribe(String localeCode, String sectionName, String tribeCode) throws IOException, TemplateException {
		System.out.println("Generating ECO for " + localeCode + " from " + sectionName);
		ConfSection section = widelandsConf.getTribe(tribeCode).getSection(sectionName);
		for (String typeCode : section.getKeys()) {
			root.put("typeCode", typeCode);
			System.out.println("Generating ECO for " + localeCode + " from " + sectionName + " with " + typeCode);
			Template temp = cfg.getTemplate(LEVEL_LOCALE_TRIBE + sectionName + "-eco.ftl");
			File docDir = new File(installationDir, TARGETDIR_HTML + localeCode + "/" + tribeCode);
			docDir.mkdirs();
			Writer out = new FileWriter(new File(docDir, sectionName + "_" + typeCode + ".html"));
			temp.process(root, out);
			out.flush();
		}

	}

	//	private void generateDotLocaleTribe(String localeCode, String sectionName, String tribeCode) throws IOException, TemplateException {
	//		System.out.println("Generating DOT for " + localeCode + " from " + sectionName);
	//		ConfSection section = widelandsConf.getTribe(tribeCode).getSection(sectionName);
	//		for (String typeCode : section.getKeys()) {
	//			root.put("typeCode", typeCode);
	//			System.out.println("Generating DOT for " + localeCode + " from " + sectionName + " with " + typeCode);
	//			Template temp = cfg.getTemplate(LEVEL_LOCALE_TRIBE + sectionName + "-dot.ftl");
	//			File dotDir = new File(installationDir, TARGETDIR_DOT + localeCode + "/" + tribeCode);
	//			File pngDir = new File(installationDir, TARGETDIR_HTML + localeCode + "/" + tribeCode);
	//			dotDir.mkdirs();
	//			pngDir.mkdirs();
	//			Writer out = new FileWriter(new File(dotDir, sectionName + "_" + typeCode + ".dot"));
	//			temp.process(root, out);
	//			out.flush();
	//			List<String> command = new ArrayList<String>();
	//			command.add("dot_static");
	//			//command.add("-v");
	//			command.add("-Gsize=5,5");
	//			//command.add("-s100");
	//			command.add("-Tpng");
	//			command.add("-o" + pngDir.getAbsolutePath() + "/" + sectionName + "_" + typeCode + ".png");
	//			command.add(sectionName + "_" + typeCode + ".dot");
	//			Process proc = Runtime.getRuntime().exec(command.toArray(new String[0]), null, dotDir);
	//			// any error message?
	//			StreamGobbler errorGobbler = new StreamGobbler(proc.getErrorStream(), "ERROR");
	//
	//			// any output?
	//			StreamGobbler outputGobbler = new StreamGobbler(proc.getInputStream(), "OUTPUT");
	//
	//			// kick them off
	//			errorGobbler.start();
	//			outputGobbler.start();
	//
	//			try {
	//				proc.waitFor();
	//			} catch (InterruptedException e) {
	//				// TODO Auto-generated catch block
	//				e.printStackTrace();
	//			}
	//		}
	//
	//	}

	class StreamGobbler extends Thread {

		InputStream is;
		String type;

		StreamGobbler(InputStream is, String type) {
			this.is = is;
			this.type = type;
		}

		public void run() {
			try {
				InputStreamReader isr = new InputStreamReader(is);
				BufferedReader br = new BufferedReader(isr);
				String line = null;
				while ((line = br.readLine()) != null)
					System.out.println(type + ">" + line);
			} catch (IOException ioe) {
				ioe.printStackTrace();
			}
		}
	}

}
