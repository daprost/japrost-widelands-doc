package de.japrost.widelands.out;

public class FreeMarkerTools {

	public static String escapeDotCode(String name) {
		return name.replace('-', '_');
	}

	public static String wareTypeColor() {
		return "white";
	}

	public static String productionSiteColor() {
		return "gold";
	}

	public static String trainingSiteColor() {
		return "aquamarine";
	}
}
