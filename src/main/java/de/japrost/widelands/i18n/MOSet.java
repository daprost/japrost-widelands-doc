package de.japrost.widelands.i18n;

import gnu.gettext.MOParser;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import de.japrost.widelands.in.TribeConf;
import de.japrost.widelands.in.WidelandsConf;

public class MOSet {

	private File localeDir;
	private Map<String, TribeMessages> tribeMessagesMap = new HashMap<String, TribeMessages>();
	private Messages widelandsMessages = new WidelandsMessages();
	private Messages customMessages = new CustomMessages();

	public void setTribeMessagesMap(Map<String, TribeMessages> tribeMessages) {
		this.tribeMessagesMap = tribeMessages;
	}

	public Map<String, TribeMessages> getTribeMessagesMap() {
		return tribeMessagesMap;
	}

	public void setLocaleDir(File localeDir) {
		this.localeDir = localeDir;
	}

	public File getLocaleDir() {
		return localeDir;
	}

	public void read(WidelandsConf conf) {
		for (String tribeCode : conf.getTribeCodes()) {
			File tribeFile = new File(localeDir, "tribe_" + tribeCode + ".mo");
			MOParser moParser = new MOParser();
			moParser.parse(tribeFile.getAbsolutePath());
			TribeMessages tribeMessages = new TribeMessages();
			tribeMessages.setMessages(moParser.getMap());
			this.tribeMessagesMap.put(tribeCode, tribeMessages);
		}
		File wiedlandsFile = new File(localeDir, "widelands" + ".mo");
		MOParser moParser = new MOParser();
		moParser.parse(wiedlandsFile.getAbsolutePath());
		widelandsMessages.setMessages(moParser.getMap());
		// fill in custom messages

		Map<String, String> messages = new HashMap<String, String>();
		messages.put(TribeConf.SECTION_INITIALIZATIONS, widelandsMessages._("_Initialization"));
		messages.put(TribeConf.SECTION_MILITARYSITE_TYPES, widelandsMessages._("_Military"));
		messages.put(TribeConf.SECTION_PRODUCTIONSITE_TYPES, widelandsMessages._("_Buildings"));
		messages.put(TribeConf.SECTION_TRAININGSITE_TYPES, widelandsMessages._("_Trainingsite"));
		messages.put(TribeConf.SECTION_WARE_TYPES, widelandsMessages._("_Ware types"));
		messages.put(TribeConf.SECTION_WAREHOUSE_TYPES, widelandsMessages._("_Warehouse"));
		messages.put(TribeConf.SECTION_WORKER_TYPES, widelandsMessages._("_Worker types"));

		customMessages.setMessages(messages);
	}

	public String _(String set, String key) {
		// TODO use set
		//System.out.println("Looking for '" + key + "' in set '" + set + "'");
		if (set.startsWith("tribe_")) {
			Messages tribeMessages = tribeMessagesMap.get(set.substring(6));
			if (tribeMessages == null) {
				return key.toUpperCase() + "[MO]";
			}
			return tribeMessages._(key);
		} else if (set.equals("widelands")) {
			return widelandsMessages._(key);
		} else if (set.equals("custom")) {
			return customMessages._(key);
		}
		return key.toUpperCase() + "[SET]";
	}

}
