package de.japrost.widelands.i18n;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import de.japrost.widelands.in.WidelandsConf;

public class WidelandsLC {

	private File installationDir;

	private Map<String, MOSet> localeMap = new TreeMap<String, MOSet>();

	public void setLocaleMap(Map<String, MOSet> localeMap) {
		this.localeMap = localeMap;
	}

	public Map<String, MOSet> getLocaleMap() {
		return localeMap;
	}

	public void setInstallationDir(File installationDir) {
		this.installationDir = installationDir;
	}

	public File getInstallationDir() {
		return installationDir;
	}

	public void readLocales(WidelandsConf conf) {
		File localesDir = new File(installationDir, "locale");
		File[] locales = localesDir.listFiles();
		for (File file : locales) {
			readLocale(file.getName(), conf);
		}

	}

	public void readLocale(String localeCode, WidelandsConf conf) {
		File localeDir = new File(installationDir, "locale/" + localeCode + "/LC_MESSAGES/");
		System.out.println("Reading " + localeDir.getAbsolutePath());
		MOSet moSet = new MOSet();
		localeMap.put(localeCode, moSet);
		moSet.setLocaleDir(localeDir);
		moSet.read(conf);

	}

	// read methods
	public List<String> getLocaleCodes() {
		List<String> result = new ArrayList<String>();
		for (String key : localeMap.keySet()) {
			result.add(key);
		}
		return result;
	}

	public String _(String localeCode, String set, String key) {
		MOSet moSet = localeMap.get(localeCode);
		if (moSet == null) {
			return key.toUpperCase() + "[LC]";
		}
		return moSet._(set, key);
	}

}
