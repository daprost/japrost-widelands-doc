package de.japrost.widelands.i18n;

import java.util.Map;

public class Messages {

	private Map<String, String> messages;

	public Messages() {
		super();
	}

	public void setMessages(Map<String, String> messages) {
		this.messages = messages;
	}

	public Map<String, String> getMessages() {
		return messages;
	}

	public String _(String key) {
		String realKey = key;
		if (realKey.startsWith("_")) {
			realKey = realKey.substring(1);
		} else {
			return key.toUpperCase() + "[NO]";
			//return ""; // TODO ???
		}
		if (realKey.startsWith(" ")) {
			realKey = realKey.substring(1);
		}
		if (realKey.startsWith("\"")) {
			realKey = realKey.substring(1, realKey.length() - 1).trim();
		}
		String value = messages.get(realKey);
		if (value == null) {
			return key.toUpperCase() + "[MSG]";
		}
		return value;
	}

}