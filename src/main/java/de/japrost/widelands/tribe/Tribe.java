package de.japrost.widelands.tribe;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class Tribe extends AbstractType {

	private Map<String, WorkerType> workerTypes = new HashMap<String, WorkerType>();;
	private Map<String, ProductionSiteType> productionSiteTypes = new HashMap<String, ProductionSiteType>();
	// ignore the defaults (are they colors)

	// [tribe]
	private String author;
	// name is inherited
	private String description;
	// bob vision range ??
	// ui position ??
	private String carrier2;

	// [frontier]
	// currently unused
	// [flag]
	// currently unused
	// [immovable types]
	// currently not interesting
	// [critter bob types] -> viehzeug
	// currently not interesting
	// [ware types] 
	private Map<String, WareType> wareTypes = new HashMap<String, WareType>();

	public void setWareTypes(Map<String, WareType> wareTypes) {
		this.wareTypes = wareTypes;
	}

	public Map<String, WareType> getWareTypes() {
		return wareTypes;
	}

	public final Collection<WareType> getWareTypesList() {
		return wareTypes.values();
	}

	/**
	 * @return the workerTypes
	 */
	public final Map<String, WorkerType> getWorkerTypes() {
		return workerTypes;
	}

	/**
	 * @param workerTypes the workerTypes to set
	 */
	public final void setWorkerTypes(Map<String, WorkerType> workerTypes) {
		this.workerTypes = workerTypes;
	}

	/**
	 * @return the productionSiteTypes
	 */
	public final Map<String, ProductionSiteType> getProductionSiteTypes() {
		return productionSiteTypes;
	}

	public final Collection<ProductionSiteType> getProductionSiteTypesList() {
		return productionSiteTypes.values();
	}

	/**
	 * @param productionSiteTypes the productionSiteTypes to set
	 */
	public final void setProductionSiteTypes(Map<String, ProductionSiteType> productionSiteTypes) {
		this.productionSiteTypes = productionSiteTypes;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public final String getAuthor() {
		return author;
	}

	public final void setAuthor(String author) {
		this.author = author;
	}

	public final String getCarrier2() {
		return carrier2;
	}

	public final void setCarrier2(String carrier2) {
		this.carrier2 = carrier2;
	}
}
