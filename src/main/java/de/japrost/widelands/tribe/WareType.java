package de.japrost.widelands.tribe;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class WareType extends AbstractType {

	int preciousness;
	int defaultTargetQuantity;
	private String help;
	private PictureSet idle;

	/**
	 * @return the preciousness
	 */
	public final int getPreciousness() {
		return preciousness;
	}

	/**
	 * @param preciousness the preciousness to set
	 */
	public final void setPreciousness(int preciousness) {
		this.preciousness = preciousness;
	}

	/**
	 * @return the defaultTargetQuantity
	 */
	public final int getDefaultTargetQuantity() {
		return defaultTargetQuantity;
	}

	/**
	 * @param defaultTargetQuantity the defaultTargetQuantity to set
	 */
	public final void setDefaultTargetQuantity(int defaultTargetQuantity) {
		this.defaultTargetQuantity = defaultTargetQuantity;
	}

	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
	}

	public void setIdle(PictureSet idle) {
		this.idle = idle;
	}

	public PictureSet getIdle() {
		return idle;
	}

	public void setHelp(String help) {
		this.help = help;
	}

	public String getHelp() {
		return help;
	}
}
