package de.japrost.widelands.tribe;

import java.io.File;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration2;

public abstract class AbstractTypeReader<TYPE extends AbstractType> {

	File baseDir;

	private final String confFileName = "conf";

	protected TYPE type;

	/**
	 * @return the baseDir
	 */
	public final File getBaseDir() {
		return baseDir;
	}

	/**
	 * @param baseDir the baseDir to set
	 */
	public final void setBaseDir(File baseDir) {
		this.baseDir = baseDir;
	}

	public void readType(TYPE type) {
		System.out.println("ATR: START reading type " + type.getCode());
		this.type = type;
		try {
			HierarchicalINIConfiguration2 configuration = new HierarchicalINIConfiguration2(new File(baseDir, confFileName));
			// debug sections
			//			for (Iterator i = configuration.getSections().iterator(); i.hasNext();) {
			//				System.out.println("[" + i.next() + "]");
			//
			//			}
			doReadType(configuration);
		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			System.out.println("ATR: FAIL  reading type " + type.getCode());
			e.printStackTrace();
		}
		System.out.println("ATR: END   reading type " + type.getCode());

	}

	protected static String i18nConvert(String s) {
		if (s.startsWith("_")) {
			s = s.substring(1);
		}
		if (s.startsWith("\"")) {
			s = s.substring(1, s.length() - 1);
		}
		return s;
	}

	protected abstract void doReadType(HierarchicalINIConfiguration2 configuration);

	public abstract TYPE newTypeInstance();
}
