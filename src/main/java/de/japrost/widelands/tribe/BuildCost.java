package de.japrost.widelands.tribe;

public class BuildCost {

	// todo does it matter what type?
	String code;
	Integer amount;

	/**
	 * @return the code
	 */
	public final String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public final void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the amount
	 */
	public final Integer getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public final void setAmount(Integer amount) {
		this.amount = amount;
	}
}
