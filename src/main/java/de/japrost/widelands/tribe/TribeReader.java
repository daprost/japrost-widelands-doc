package de.japrost.widelands.tribe;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration2;
import org.apache.commons.configuration.SubnodeConfiguration;

public class TribeReader {

	private File baseDir;
	private final String confFileName = "conf";

	private final String SECTION_TRIBE = "tribe";
	private final String SECTION_TRIBE_AUTHOR = "author";
	private final String SECTION_TRIBE_DESCRIPTION = "descr";
	private final String SECTION_TRIBE_CARRIER2 = "carrier2";
	private final String SECTION_WARE_TYPES = "ware types";
	private final String SECTION_WORKER_TYPES = "worker types";
	private static final String SECTION_PRODUCTIONSITES_TYPES = "productionsite types";

	public void setBaseDir(File baseDir) {
		this.baseDir = baseDir;
	}

	public File getBaseDir() {
		return baseDir;
	}

	public Tribe readTribe(String tribeCode) {
		System.out.println("TR: START reading tribe " + tribeCode);
		Tribe tribe = new Tribe();
		try {
			HierarchicalINIConfiguration2 configuration = new HierarchicalINIConfiguration2(new File(baseDir, confFileName));
			// main section
			SubnodeConfiguration tribeSection = configuration.getSection(SECTION_TRIBE);
			tribe.setName(AbstractTypeReader.i18nConvert(tribeSection.getString("name")));
			tribe.setAuthor(AbstractTypeReader.i18nConvert(tribeSection.getString(SECTION_TRIBE_AUTHOR)));
			tribe.setDescription(AbstractTypeReader.i18nConvert(tribeSection.getString(SECTION_TRIBE_DESCRIPTION)));
			tribe.setCarrier2(AbstractTypeReader.i18nConvert(tribeSection.getString(SECTION_TRIBE_CARRIER2)));
			// ware types
			tribe.setWareTypes(readType(configuration, SECTION_WARE_TYPES, new WareTypeReader()));
			// worker types
			tribe.setWorkerTypes(readType(configuration, SECTION_WORKER_TYPES, new WorkerTypeReader()));
			// production site types
			tribe.setProductionSiteTypes(readType(configuration, SECTION_PRODUCTIONSITES_TYPES, new ProductionSiteTypeReader()));

		} catch (ConfigurationException e) {
			// TODO Auto-generated catch block
			System.out.println("TR: FAIL  reading tribe " + tribeCode);
			e.printStackTrace();
			return null;
		}
		System.out.println("TR: END   reading tribe " + tribeCode);
		return tribe;
	}

	private <TYPE extends AbstractType> Map<String, TYPE> readType(HierarchicalINIConfiguration2 configuration, String section,
			AbstractTypeReader<TYPE> abstractTypeReader) {
		SubnodeConfiguration typesSection = configuration.getSection(section);
		Map<String, TYPE> result = new HashMap<String, TYPE>();
		for (Iterator i = typesSection.getKeys(); i.hasNext();) {
			String typeCode = (String) i.next();
			String typeName = typesSection.getString(typeCode).substring(1);
			if (typeName.startsWith("\"")) {
				typeName = typeName.substring(1, typeName.length() - 1);
			}
			abstractTypeReader.setBaseDir(new File(baseDir, typeCode));
			TYPE type = abstractTypeReader.newTypeInstance();
			type.setCode(typeCode);
			type.setName(typeName);
			abstractTypeReader.readType(type);
			result.put(typeCode, type);

		}
		return result;
	}
}
