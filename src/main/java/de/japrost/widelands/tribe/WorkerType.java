package de.japrost.widelands.tribe;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

public class WorkerType extends AbstractType {

	private BuildCosts buildCost;

	// TODO more fields

	public String toString() {
		return ReflectionToStringBuilder.toString(this, ToStringStyle.SIMPLE_STYLE);
	}

	public void setBuildCost(BuildCosts buildCost) {
		this.buildCost = buildCost;
	}

	public BuildCosts getBuildCost() {
		return buildCost;
	}
}
