package de.japrost.widelands.tribe;

import org.apache.commons.configuration.HierarchicalINIConfiguration2;

public class WorkerTypeReader extends AbstractTypeReader<WorkerType> {

	private final String SECTION_BUILDCOST = "buildcost";
	// konfiguration in den wirtschaftseinstellungen
	private final String KEY_DEFAULT_TARGET_QUANTITY = "default_target_quantity";

	@Override
	protected void doReadType(HierarchicalINIConfiguration2 configuration) {
		type.setBuildCost(BuildCostParser.parseBuildCost(configuration.getSection(SECTION_BUILDCOST)));
	}

	@Override
	public WorkerType newTypeInstance() {
		return new WorkerType();
	}
}
