package de.japrost.widelands.tribe;

import org.apache.commons.configuration.HierarchicalINIConfiguration2;

public class ProductionSiteTypeReader extends AbstractTypeReader<ProductionSiteType> {

	private final String SECTION_BUILDCOST = "buildcost";
	private final String SECTION_IDLE = "idle";

	@Override
	protected void doReadType(HierarchicalINIConfiguration2 configuration) {
		type.setBuildCost(BuildCostParser.parseBuildCost(configuration.getSection(SECTION_BUILDCOST)));
		type.setIdle(PictureSetParser.parsePictureSet(configuration.getSection(SECTION_IDLE)));

	}

	@Override
	public ProductionSiteType newTypeInstance() {
		return new ProductionSiteType();
	}

}
