package de.japrost.widelands.tribe;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class BuildCosts {

	private Map<String, BuildCost> costs = new HashMap<String, BuildCost>();

	public void setCosts(Map<String, BuildCost> costs) {
		this.costs = costs;
	}

	public Map<String, BuildCost> getCosts() {
		return costs;
	}

	public Collection<BuildCost> getBuildCosts() {
		return costs.values();
	}
}
