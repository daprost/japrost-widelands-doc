package de.japrost.widelands.tribe;

import org.apache.commons.configuration.SubnodeConfiguration;

public class PictureSetParser {

	public static PictureSet parsePictureSet(SubnodeConfiguration configuration) {
		PictureSet result = new PictureSet();
		result.setPics(configuration.getString("pics"));
		result.setHotSpot(configuration.getString("hotspot"));
		return result;
	}
}
