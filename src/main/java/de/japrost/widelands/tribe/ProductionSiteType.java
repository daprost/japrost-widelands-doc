package de.japrost.widelands.tribe;

public class ProductionSiteType extends AbstractType {

	private BuildCosts buildCost;
	private PictureSet idle;

	public void setBuildCost(BuildCosts buildCost) {
		this.buildCost = buildCost;
	}

	public BuildCosts getBuildCost() {
		return buildCost;
	}

	/**
	 * @return the idle
	 */
	public final PictureSet getIdle() {
		return idle;
	}

	/**
	 * @param idle the idle to set
	 */
	public final void setIdle(PictureSet idle) {
		this.idle = idle;
	}

}
