package de.japrost.widelands.tribe;

import java.util.Iterator;

import org.apache.commons.configuration.SubnodeConfiguration;

public class BuildCostParser {

	public static BuildCosts parseBuildCost(SubnodeConfiguration configuration) {
		BuildCosts buildCosts = new BuildCosts();
		for (Iterator i = configuration.getKeys(); i.hasNext();) {
			String code = (String) i.next();
			BuildCost buildCost = new BuildCost();
			buildCost.setCode(code);
			buildCost.setAmount(configuration.getInteger(buildCost.getCode(), null));
			buildCosts.getCosts().put(buildCost.getCode(), buildCost);
		}
		return buildCosts;
	}
}
