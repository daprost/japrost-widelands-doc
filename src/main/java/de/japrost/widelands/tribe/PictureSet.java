package de.japrost.widelands.tribe;

public class PictureSet {

	private String pics;
	private String hotSpot;

	/**
	 * @return the pics
	 */
	public final String getPics() {
		return pics;
	}

	/**
	 * @param pics the pics to set
	 */
	public final void setPics(String pics) {
		this.pics = pics;
	}

	/**
	 * @return the hotSpot
	 */
	public final String getHotSpot() {
		return hotSpot;
	}

	/**
	 * @param hotSpot the hotSpot to set
	 */
	public final void setHotSpot(String hotSpot) {
		this.hotSpot = hotSpot;
	}

	public String getFirstPic() {
		return pics.replace('?', '0');
	}
}
