package de.japrost.widelands.tribe;

import org.apache.commons.configuration.HierarchicalINIConfiguration2;

public class WareTypeReader extends AbstractTypeReader<WareType> {

	// wie wichtig ist der kram
	private final String KEY_PRECOUSNESS = "preciousness";
	// konfiguration in den wirtschaftseinstellungen
	private final String KEY_DEFAULT_TARGET_QUANTITY = "default_target_quantity";
	private final String KEY_HELP = "help";
	private final String SECTION_IDLE = "idle";

	@Override
	protected void doReadType(HierarchicalINIConfiguration2 configuration) {
		type.setPreciousness(configuration.getInt(KEY_PRECOUSNESS, -1));
		type.setDefaultTargetQuantity(configuration.getInt(KEY_DEFAULT_TARGET_QUANTITY, -1));
		type.setHelp(i18nConvert(configuration.getString(KEY_HELP, "No Help available.")));

		type.setIdle(PictureSetParser.parsePictureSet(configuration.getSection(SECTION_IDLE)));
	}

	@Override
	public WareType newTypeInstance() {
		return new WareType();
	}
}
