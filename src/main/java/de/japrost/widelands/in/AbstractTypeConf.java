package de.japrost.widelands.in;

public class AbstractTypeConf extends Conf {

	public String getFirstIdlePic() {
		String pic = this.get("idle", "pics");
		if (pic == null) {
			return "[NO PIC AVAILABLE]";
		}
		return pic.replace('?', '0');
	}
}
