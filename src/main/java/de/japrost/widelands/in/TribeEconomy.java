package de.japrost.widelands.in;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class TribeEconomy {

	public TribeEconomy(TribeConf tribeConf) {
		ConfSection sectionWT = tribeConf.getSection(TribeConf.SECTION_WARE_TYPES);
		for (String wareTypeCode : sectionWT.getKeys()) {
			// for each wareTypeCode
			List<EconomyEntry> productionCodes = new ArrayList<EconomyEntry>();
			wareTypeProduction.put(wareTypeCode, productionCodes);
			List<EconomyEntry> consumptionCodes = new ArrayList<EconomyEntry>();
			wareTypeConsumption.put(wareTypeCode, consumptionCodes);

			// get production sites that use the ware type
			extractWareTypeInfo(tribeConf, wareTypeCode, productionCodes, consumptionCodes, TribeConf.SECTION_PRODUCTIONSITE_TYPES);
			// get production sites that use the ware type
			extractWareTypeInfo(tribeConf, wareTypeCode, productionCodes, consumptionCodes, TribeConf.SECTION_TRAININGSITE_TYPES);
			// get the worker type that use the ware type
			// FIXME consumption codes need to know where they are from!
			//extractWareTypeWorkerInfo(tribeConf, wareTypeCode, consumptionCodes);
		}

	}

	private void extractWareTypeInfo(TribeConf tribeConf, String wareTypeCode, List<EconomyEntry> productionCodes, List<EconomyEntry> consumptionCodes,
			String sectionCode) {
		ConfSection section = tribeConf.getSection(sectionCode);
		for (String buildingCode : section.getKeys()) {
			Conf subConf = tribeConf.getSubConf(sectionCode, buildingCode);
			List<String> listOut = subConf.getList("#default#", "output");
			if (listOut != null) {
				if (listOut.contains(wareTypeCode)) {
					EconomyEntry e = new EconomyEntry(sectionCode, buildingCode);
					productionCodes.add(e);
					putProduction(wareTypeCode, buildingCode);
				}
			}
			if (subConf.getSection("inputs") != null) {
				List<String> listIn = subConf.getSection("inputs").getKeys();
				if (listIn != null) {
					if (listIn.contains(wareTypeCode)) {
						EconomyEntry e = new EconomyEntry(sectionCode, buildingCode);
						consumptionCodes.add(e);
						putConsumption(wareTypeCode, buildingCode);
					}
				}
			}
		}
	}

	private void putProduction(String wareTypeCode, String buildingCode) {
		List<EconomyEntry> wareTypes = buildingProduction.get(buildingCode);
		if (wareTypes == null) {
			wareTypes = new ArrayList<EconomyEntry>();
			buildingProduction.put(buildingCode, wareTypes);
		}
		EconomyEntry e = new EconomyEntry(TribeConf.SECTION_WARE_TYPES, wareTypeCode);
		if (!wareTypes.contains(e)) {
			wareTypes.add(e);
		}
	}

	private void putConsumption(String wareTypeCode, String buildingCode) {
		List<EconomyEntry> wareTypes = buildingConsumption.get(buildingCode);
		if (wareTypes == null) {
			wareTypes = new ArrayList<EconomyEntry>();
			buildingConsumption.put(buildingCode, wareTypes);
		}
		EconomyEntry e = new EconomyEntry(TribeConf.SECTION_WARE_TYPES, wareTypeCode);
		if (!wareTypes.contains(e)) {
			wareTypes.add(e);
		}
	}

	private void extractWareTypeWorkerInfo(TribeConf tribeConf, String wareTypeCode, List<String> consumptionCodes) {
		ConfSection section = tribeConf.getSection(TribeConf.SECTION_WORKER_TYPES);
		for (String workerCode : section.getKeys()) {
			Conf subConf = tribeConf.getSubConf(TribeConf.SECTION_WORKER_TYPES, workerCode);
			if (subConf.getSection("buildcost") != null) {
				List<String> listIn = subConf.getSection("buildcost").getKeys();
				if (listIn != null) {
					if (listIn.contains(wareTypeCode)) {
						consumptionCodes.add(workerCode);
					}
				}
			}
		}
	}

	// Economy of WareTypes:
	//   Production of WareType
	//     BuildingTypeCode
	//   Consumption of WareType
	//     BuildingTypeCode
	private Map<String, List<EconomyEntry>> wareTypeProduction = new TreeMap<String, List<EconomyEntry>>();
	private Map<String, List<EconomyEntry>> wareTypeConsumption = new TreeMap<String, List<EconomyEntry>>();
	// Economy of Buildings:
	//   Production of BuildingTypeCode
	//     WareTypeCode
	//   Consumption of BuildingTypeCode
	//     WareTypeCode
	private Map<String, List<EconomyEntry>> buildingProduction = new TreeMap<String, List<EconomyEntry>>();
	private Map<String, List<EconomyEntry>> buildingConsumption = new TreeMap<String, List<EconomyEntry>>();

	public List<EconomyEntry> getProductionBuildings(String wareType) {
		return wareTypeProduction.get(wareType) != null ? wareTypeProduction.get(wareType) : new ArrayList<EconomyEntry>();
	}

	public List<EconomyEntry> getConsumptionBuildings(String wareType) {
		return wareTypeConsumption.get(wareType) != null ? wareTypeConsumption.get(wareType) : new ArrayList<EconomyEntry>();
	}

	public List<EconomyEntry> getProductionWareTypes(String building) {
		return buildingProduction.get(building) != null ? buildingProduction.get(building) : new ArrayList<EconomyEntry>();
	}

	public List<EconomyEntry> getConsumptionWareTypes(String building) {
		return buildingConsumption.get(building) != null ? buildingConsumption.get(building) : new ArrayList<EconomyEntry>();
	}

}
