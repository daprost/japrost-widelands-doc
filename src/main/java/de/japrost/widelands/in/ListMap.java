package de.japrost.widelands.in;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ListMap<K, V> implements Map<K, List<V>> {

	private Map<K, List<V>> c = new HashMap<K, List<V>>();

	// own stuff
	/**
	 * Add the value to the list for the key. <code>null</code>-safe.
	 * 
	 * @param key
	 * @param value
	 */
	public void add(K key, V value) {
		List<V> l = c.get(key);
		if (l == null) {
			l = new ArrayList<V>();
			c.put(key, l);
		}
		if (!l.contains(value)) {
			l.add(value);
		}
	}

	public V getSingleValue(K key) {
		List<V> l = c.get(key);
		if (l == null) {
			return null;
		}
		if (l.size() == 0) {
			return null;
		}
		return l.get(0);
	}

	/**
	 * Return true if the key exists and there is exactly one value for the key.
	 * 
	 * @param key
	 * @return
	 */
	public boolean isValueList(K key) {
		List<V> l = c.get(key);
		if (l == null) {
			return false;
		}
		System.out.println("Hier:" + key + "-(" + l.size() + ")>" + l);
		if (l.size() > 1) {
			return true;
		}
		return false;
	}

	// DELEGATORS
	public String toString() {
		return c.toString();
	}

	public void clear() {
		c.clear();
	}

	public boolean containsKey(Object key) {
		return c.containsKey(key);
	}

	public boolean containsValue(Object value) {
		return c.containsValue(value);
	}

	public Set<java.util.Map.Entry<K, List<V>>> entrySet() {
		return c.entrySet();
	}

	public boolean equals(Object o) {
		return c.equals(o);
	}

	public List<V> get(Object key) {
		return c.get(key);
	}

	public int hashCode() {
		return c.hashCode();
	}

	public boolean isEmpty() {
		return c.isEmpty();
	}

	public Set<K> keySet() {
		return c.keySet();
	}

	public List<V> put(K key, List<V> value) {
		return c.put(key, value);
	}

	public void putAll(Map<? extends K, ? extends List<V>> m) {
		c.putAll(m);
	}

	public List<V> remove(Object key) {
		return c.remove(key);
	}

	public int size() {
		return c.size();
	}

	public Collection<List<V>> values() {
		return c.values();
	}
}
