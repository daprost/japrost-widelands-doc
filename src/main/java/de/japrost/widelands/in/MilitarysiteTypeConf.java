package de.japrost.widelands.in;

public class MilitarysiteTypeConf extends AbstractTypeConf {

	public int getVisionRange() {
		String vr = this.getSection("#default#").getSingleValue("vision_range");
		if (vr != null) {
			return Integer.parseInt(vr);
		}
		String con = this.getSection("#default#").getSingleValue("conquers");
		if (con == null) {
			return -1;
		}
		//logic/building.cc:229:  return m_vision_range ? m_vision_range : get_conquers() + 4;
		return Integer.parseInt(con) + 4;
	}
}
