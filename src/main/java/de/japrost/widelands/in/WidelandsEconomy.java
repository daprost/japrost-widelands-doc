package de.japrost.widelands.in;

import java.util.Map;
import java.util.TreeMap;

public class WidelandsEconomy {

	// Contents
	private Map<String, TribeEconomy> tribeEco = new TreeMap<String, TribeEconomy>();

	public WidelandsEconomy(WidelandsConf widelandsConf) {
		for (String tribeCode : widelandsConf.getTribeCodes()) {
			TribeConf tribeConf = widelandsConf.getTribe(tribeCode);
			TribeEconomy tribeEconomy = new TribeEconomy(tribeConf);
			tribeEco.put(tribeCode, tribeEconomy);
		}

	}

	public TribeEconomy getTribeEconomy(String tribeCode) {
		TribeEconomy result = tribeEco.get(tribeCode);
		if (result == null) {
			System.out.println("Mist" + tribeCode);
			System.exit(0);
		}
		return result;
	}
}
