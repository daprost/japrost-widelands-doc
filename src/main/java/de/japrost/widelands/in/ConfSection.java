package de.japrost.widelands.in;

import java.util.ArrayList;
import java.util.List;

public class ConfSection {

	private ListMap<String, String> contents = new ListMap<String, String>();

	public void setContents(ListMap<String, String> contents) {
		this.contents = contents;
	}

	public ListMap<String, String> getContents() {
		return contents;
	}

	public List<String> getKeys() {
		List<String> result = new ArrayList<String>();
		for (String key : contents.keySet()) {
			result.add(key);
		}
		return result;

	}

	public String getSingleValue(String key) {
		return contents.getSingleValue(key);
	}

	public int getSingleInt(String key) {
		try {
			return Integer.parseInt(contents.getSingleValue(key));
		} catch (NumberFormatException e) {
			System.out.println(key + "->" + e.getMessage());
			return Integer.MIN_VALUE;
		}
	}

	public boolean isValueList(String key) {
		return contents.isValueList(key);
	}

	public List<String> getValueList(String key) {
		return contents.get(key);
	}
}
