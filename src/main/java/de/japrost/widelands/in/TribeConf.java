package de.japrost.widelands.in;

import java.io.File;
import java.util.Map;
import java.util.TreeMap;

public class TribeConf extends Conf {

	// defaults -> no further
	// tribe -> no further? (carrier2)
	// frontier -> no further?
	// flag -> no further?
	// !immovable types
	// !ware types
	public static final String SECTION_WARE_TYPES = "ware types";
	private Map<String, WareTypeConf> wareTypeConfs = new TreeMap<String, WareTypeConf>();
	// !carrier types
	// !soldier types
	// !worker types
	public static final String SECTION_WORKER_TYPES = "worker types";
	private Map<String, WorkerTypeConf> workerTypeConfs = new TreeMap<String, WorkerTypeConf>();

	// !constructionsite types
	// !trainingsite types
	public static final String SECTION_TRAININGSITE_TYPES = "trainingsite types";
	private Map<String, TrainingsiteTypeConf> trainingsiteTypeConfs = new TreeMap<String, TrainingsiteTypeConf>();
	// !militarysite types
	public static final String SECTION_MILITARYSITE_TYPES = "militarysite types";
	private Map<String, MilitarysiteTypeConf> militarysiteTypeConfs = new TreeMap<String, MilitarysiteTypeConf>();
	// !global militarysite types
	// !warehouse types
	public static final String SECTION_WAREHOUSE_TYPES = "warehouse types";
	private Map<String, WarehouseTypeConf> warehouseTypeConfs = new TreeMap<String, WarehouseTypeConf>();
	// !productionsite types
	public static final String SECTION_PRODUCTIONSITE_TYPES = "productionsite types";
	private Map<String, ProductionsiteTypeConf> productionsiteTypeConfs = new TreeMap<String, ProductionsiteTypeConf>();
	// !	initializations
	public static final String SECTION_INITIALIZATIONS = "initializations";
	private Map<String, InitializationsConf> initializationsConfs = new TreeMap<String, InitializationsConf>();

	// military data --> no further

	@Override
	protected void loadSubConf() {
		ConfSection workerTypes = contents.get(SECTION_WORKER_TYPES);
		for (String workerTypeCode : workerTypes.getKeys()) {
			WorkerTypeConf workerTypeConf = new WorkerTypeConf();
			workerTypeConf.setConfDir(new File(this.confDir, workerTypeCode));
			workerTypeConf.load();
			workerTypeConfs.put(workerTypeCode, workerTypeConf);
		}
		ConfSection wareTypes = contents.get(SECTION_WARE_TYPES);
		for (String workerTypeCode : wareTypes.getKeys()) {
			WareTypeConf subConf = new WareTypeConf();
			subConf.setConfDir(new File(this.confDir, workerTypeCode));
			subConf.load();
			wareTypeConfs.put(workerTypeCode, subConf);
		}
		ConfSection productionsiteTypes = contents.get(SECTION_PRODUCTIONSITE_TYPES);
		for (String code : productionsiteTypes.getKeys()) {
			ProductionsiteTypeConf subConf = new ProductionsiteTypeConf();
			subConf.setConfDir(new File(this.confDir, code));
			subConf.load();
			productionsiteTypeConfs.put(code, subConf);
		}
		if (true) {
			ConfSection types = contents.get(SECTION_MILITARYSITE_TYPES);
			for (String code : types.getKeys()) {
				MilitarysiteTypeConf subConf = new MilitarysiteTypeConf();
				subConf.setConfDir(new File(this.confDir, code));
				subConf.load();
				militarysiteTypeConfs.put(code, subConf);
			}
		}
		if (true) {
			ConfSection types = contents.get(SECTION_TRAININGSITE_TYPES);
			for (String code : types.getKeys()) {
				TrainingsiteTypeConf subConf = new TrainingsiteTypeConf();
				subConf.setConfDir(new File(this.confDir, code));
				subConf.load();
				trainingsiteTypeConfs.put(code, subConf);
			}
		}
		if (true) {
			ConfSection types = contents.get(SECTION_WAREHOUSE_TYPES);
			for (String code : types.getKeys()) {
				WarehouseTypeConf subConf = new WarehouseTypeConf();
				subConf.setConfDir(new File(this.confDir, code));
				subConf.load();
				warehouseTypeConfs.put(code, subConf);
			}
		}

		if (true) {
			System.out.println("Reading " + SECTION_INITIALIZATIONS);
			ConfSection types = contents.get(SECTION_INITIALIZATIONS);
			for (String code : types.getKeys()) {
				InitializationsConf subConf = new InitializationsConf();
				subConf.setConfDir(this.confDir);
				subConf.load(code);
				initializationsConfs.put(code, subConf);
			}
		}

	}

	public Conf getSubConf(String sectionCode, String confCode) {
		//System.out.println("->getSubConf(" + sectionCode + "," + confCode + ")");
		Conf conf;
		if (SECTION_WORKER_TYPES.equals(sectionCode)) {
			conf = workerTypeConfs.get(confCode);
		} else if (SECTION_WARE_TYPES.equals(sectionCode)) {
			conf = wareTypeConfs.get(confCode);
		} else if (SECTION_PRODUCTIONSITE_TYPES.equals(sectionCode)) {
			conf = productionsiteTypeConfs.get(confCode);
		} else if (SECTION_MILITARYSITE_TYPES.equals(sectionCode)) {
			conf = militarysiteTypeConfs.get(confCode);
		} else if (SECTION_INITIALIZATIONS.equals(sectionCode)) {
			conf = initializationsConfs.get(confCode);
		} else if (SECTION_TRAININGSITE_TYPES.equals(sectionCode)) {
			conf = trainingsiteTypeConfs.get(confCode);
		} else if (SECTION_WAREHOUSE_TYPES.equals(sectionCode)) {
			conf = warehouseTypeConfs.get(confCode);
		} else {
			conf = null;
		}
		//System.out.println("<-getSubConf(" + sectionCode + "," + confCode + ")=" + conf);
		return conf;
	}

	public Conf confOfBuilding(String buildingCode) {
		if (productionsiteTypeConfs.containsKey(buildingCode)) {
			return productionsiteTypeConfs.get(buildingCode);
		}
		if (militarysiteTypeConfs.containsKey(buildingCode)) {
			return militarysiteTypeConfs.get(buildingCode);
		}
		if (initializationsConfs.containsKey(buildingCode)) {
			return initializationsConfs.get(buildingCode);
		}
		if (trainingsiteTypeConfs.containsKey(buildingCode)) {
			return trainingsiteTypeConfs.get(buildingCode);
		}
		if (warehouseTypeConfs.containsKey(buildingCode)) {
			return warehouseTypeConfs.get(buildingCode);
		}
		return null;
	}
}
