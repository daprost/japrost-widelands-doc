package de.japrost.widelands.in;

public class EconomyEntry {

	public EconomyEntry() {
	}

	public EconomyEntry(String sectionCode, String typeCode) {
		super();
		this.sectionCode = sectionCode;
		this.typeCode = typeCode;
	}

	private String sectionCode;
	private String typeCode;

	public final String getSectionCode() {
		return sectionCode;
	}

	public final String getTypeCode() {
		return typeCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((sectionCode == null) ? 0 : sectionCode.hashCode());
		result = prime * result + ((typeCode == null) ? 0 : typeCode.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		EconomyEntry other = (EconomyEntry) obj;
		if (sectionCode == null) {
			if (other.sectionCode != null)
				return false;
		} else if (!sectionCode.equals(other.sectionCode))
			return false;
		if (typeCode == null) {
			if (other.typeCode != null)
				return false;
		} else if (!typeCode.equals(other.typeCode))
			return false;
		return true;
	}

}
