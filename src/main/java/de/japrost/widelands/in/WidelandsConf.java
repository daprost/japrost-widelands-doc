package de.japrost.widelands.in;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class WidelandsConf {

	private File installationDir;
	// Contents
	private Map<String, TribeConf> tribeConf = new TreeMap<String, TribeConf>();
	private String tribeConfDirName = "tribes";

	public static void main(String[] args) {
		WidelandsConf wc = new WidelandsConf();
		wc.setInstallationDir(new File("/home/uli/usr/build15/"));
		wc.readTribes();

		//		ConfReader confReader = new ConfReader();
		//		Conf conf = confReader.readConf(new File(installationDir, "tribes/barbarians/conf").getAbsolutePath());
		//		System.out.println(conf.getSection("military_data").getSingleValue("retreat"));
	}

	public void readTribe(File tribeDir) {
		System.out.println("WC: START reading tribe " + tribeDir);
		TribeConf tc = new TribeConf();
		tc.setConfDir(tribeDir);
		tribeConf.put(tribeDir.getName(), tc);
		tc.load();
		System.out.println("WC: END   reading tribe " + tribeDir);

	}

	public void readTribes() {
		File[] tribes = new File(installationDir, tribeConfDirName).listFiles();
		for (File tribeDir : tribes) {
			readTribe(tribeDir);
		}
	}

	public void setInstallationDir(File installationDir) {
		this.installationDir = installationDir;
	}

	public File getInstallationDir() {
		return installationDir;
	}

	// read methods
	public List<String> getTribeCodes() {
		List<String> result = new ArrayList<String>();
		for (String key : tribeConf.keySet()) {
			result.add(key);
		}
		return result;
	}

	public TribeConf getTribe(String tribeCode) {
		return tribeConf.get(tribeCode);
	}

	public final String getTribeConfDirName() {
		return tribeConfDirName;
	}

}
