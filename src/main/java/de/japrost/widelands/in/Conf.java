package de.japrost.widelands.in;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Conf {

	public static final String DEFAULT_SECTION_CODE = "#default#";
	protected Map<String, ConfSection> contents = new HashMap<String, ConfSection>();
	protected File confDir;

	public void setConfDir(File confDir) {
		this.confDir = confDir;
	}

	public void load() {
		ConfReader confReader = new ConfReader();
		confReader.readConf(new File(confDir, "conf").getAbsolutePath(), this);
		loadSubConf();
	}

	public void load(String confName) {
		ConfReader confReader = new ConfReader();
		confReader.readConf(new File(confDir, confName).getAbsolutePath(), this);
		loadSubConf();
	}

	protected void loadSubConf() {
	};

	public void setContents(Map<String, ConfSection> contents) {
		this.contents = contents;
	}

	public Map<String, ConfSection> getContents() {
		return contents;
	}

	public List<String> getSectionCodes() {
		List<String> result = new ArrayList<String>();
		for (String key : contents.keySet()) {
			if (key == null) {
				key = DEFAULT_SECTION_CODE;
			}
			result.add(key);
		}
		return result;
	}

	public ConfSection getSection(String sectionCode) {
		//System.out.println(confDir + "->getSection(" + sectionCode + ")");
		ConfSection result;
		if (DEFAULT_SECTION_CODE.equals(sectionCode)) {
			result = contents.get(null);
		} else {
			result = contents.get(sectionCode);
		}
		//System.out.println("<-getSection(" + sectionCode + ")=" + result);
		return result;

	}

	public String get(String sectionCode, String key) {
		ConfSection cs = getSection(sectionCode);
		if (cs == null) {
			return "NS:" + sectionCode.toUpperCase() + "[" + key.toUpperCase() + "]";
		}
		String value = cs.getSingleValue(key);
		if (value == null) {
			return "NK:" + sectionCode.toUpperCase() + "[" + key.toUpperCase() + "]";
		}
		//System.out.println(sectionCode + "," + key + "," + value);
		return value;
	}

	public List<String> getList(String section, String key) {

		ConfSection cs = getSection(section);
		if (cs == null) {
			List<String> l = new ArrayList<String>();
			l.add("NS:" + section.toUpperCase() + "[" + key.toUpperCase() + "]");
			return l;
		}
		List<String> value = cs.getValueList(key);
		if (value == null) {
			List<String> l = new ArrayList<String>();
			l.add("NK:" + section.toUpperCase() + "[" + key.toUpperCase() + "]");
			return l;
		}
		return value;

	}

	public int getSize() {
		return contents.size();
	}

	public Conf getSubConf(String sectionCode, String confCode) {
		return new Conf();
	}

}
