package de.japrost.widelands.in;

import java.io.File;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalINIConfiguration2;
import org.apache.commons.configuration.SubnodeConfiguration;

public class ConfReader {

	//	public Conf readConf(String fileName) {
	//		Conf conf = new Conf();
	//		try {
	//			HierarchicalINIConfiguration2 configuration = new HierarchicalINIConfiguration2(new File(fileName));
	//			for (Iterator<String> i = configuration.getSections().iterator(); i.hasNext();) {
	//				String sectionCode = i.next();
	//				//System.out.println("[" + sectionCode + "]");
	//				ConfSection confSection = new ConfSection();
	//				conf.getContents().put(sectionCode, confSection);
	//				readConfSection(configuration.getSection(sectionCode), confSection);
	//			}
	//		} catch (ConfigurationException e) {
	//			e.printStackTrace();
	//		}
	//		return conf;
	//	}

	public void readConf(String fileName, Conf conf) {
		System.out.println("->readConf(" + fileName + "," + conf + ")");
		try {
			HierarchicalINIConfiguration2 configuration = new HierarchicalINIConfiguration2(new File(fileName));
			for (Iterator<String> i = configuration.getSections().iterator(); i.hasNext();) {

				String sectionCode = i.next();
				System.out.println("  ->[" + sectionCode + "]");
				ConfSection confSection = new ConfSection();
				conf.getContents().put(sectionCode, confSection);
				readConfSection(configuration.getSection(sectionCode), confSection);
				System.out.println("  ->[" + sectionCode + "]=" + conf.getSection(sectionCode));

			}
		} catch (ConfigurationException e) {
			e.printStackTrace();
		}
		System.out.println("<-readConf(" + fileName + "," + conf + ")=void");
	}

	private void readConfSection(SubnodeConfiguration section, ConfSection confSection) {
		for (Iterator<String> i = section.getKeys(); i.hasNext();) {
			String key = i.next();
			if (section.getList(key).size() > 0) {
				List<String> values = (List<String>) section.getList(key);
				for (String value : values) {
					System.out.println("    ->" + key + "=" + value);
					confSection.getContents().add(key, value);
				}
			} else {
				String value = section.getString(key);
				System.out.println("    ->" + key + "=" + value);
				confSection.getContents().add(key, value);
			}
		}

	}
}
