[#ftl]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>${i18n._(tribe.name)} ${i18n._("Production")} ${i18n._("site")} </title>
</head>
<body>
<table>
[#list tribe.productionSiteTypesList as pst]
<tr>
  <td>${i18n._(pst.name)}</td>
  <td>${i18n._("Build cost")}</td>
</tr>
<tr>
  <td><img src="${baseDir}${tribeDirName}/${tribe.code}/${pst.code}/${pst.idle.firstPic}"></img></td>
  <td>
    <table>
    [#list pst.buildCost.buildCosts as cost]
      <tr><td>
      [#list 1..cost.amount as i]
      <img title="${cost.amount} ${i18n._(tribe.wareTypes[cost.code].name)}" src="${baseDir}${tribeDirName}/${tribe.code}/${cost.code}/menu.png"></img>
      [/#list]
      </td></tr>
    [/#list]
    </table>
  </td>
</tr>
[/#list]
</table>
</body>
</html>  