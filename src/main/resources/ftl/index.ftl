[#ftl]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>Widelands Fact Sheet</title>
  <link rel="stylesheet" type="text/css" href="main.css" />
  <link rel="stylesheet" type="text/css" href="custom.css" />
</head>
<body>
<table width='100%'>
  <tr>
    <td><img src="${baseDir}pics/wl-ico-128.png" alt="widelandslogo"></img></td>
    <td colspan='${tribes?size}' style="vertical-align:middle; text-align:center">Widelands Facts</td>
  </tr>
  <tr>
      <td>The Tribes</td>
    [#list tribes as tribe]
      <td><img src="${baseDir}${tribeDirName}/${tribe.code}/headquarters/headquarters_i_00.png"></img></td>
    [/#list]
  </tr>
  [#list fmConfig.locales as locale]
    <tr>
      <td>${locale}</td>
      [#list tribes as tribe]
        <td><a href="${locale}/index_${tribe.code}.html">${i18n[locale].tribeTranslations[tribe.code]._(tribe.name)}</a></td>
      [/#list]
    </tr>
  [/#list]
</table>

</body>
</html>  