[#ftl]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>${i18n._(tribe.name)} ${i18n._('Ware types')}</title>
  <link rel="stylesheet" type="text/css" href="../../main.css" />
  <link rel="stylesheet" type="text/css" href="../../custom.css" />
</head>
<body>
[#assign head_title = "${i18n._(tribe.name)} ${i18n._('Ware types')}"]
[#include "inc_head.ftl"]
<table>
<tr>
  <td>
  </td>
  <td>
  ${i18n._('Type: ')}
  </td>
  <td>
    ${i18n._('Description:')}
  </td>
</tr>
[#list tribe.wareTypesList as wareType]
<tr id="${wareType.code}">
  <td>
    <img src="${baseDir}${tribeDirName}/${tribe.code}/${wareType.code}/menu.png"></img>
  </td>
  <td>
    ${i18n._(wareType.name)}
  </td>
  <td>${i18n._(wareType.help)}</td>
</tr>
[/#list]
</table>
</body>
</html>  