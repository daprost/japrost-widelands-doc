[#ftl]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>${i18n._(tribe.name)} ${i18n._("Production")} ${i18n._("site")} </title>
  <link rel="stylesheet" type="text/css" href="../../main.css" />
  <link rel="stylesheet" type="text/css" href="../../custom.css" />
</head>
<body>
[#assign head_title = "${i18n._(tribe.name)} ${i18n._('Production')} ${i18n._('site')}"]
[#include "inc_head.ftl"]
<table>

[#list tribe.productionSiteTypesList as pst]
<tr>
  <td>${i18n._(pst.name)}</td>
  <td>${i18n._("Build cost")}</td>
</tr>
<tr>
  <td><img src="${baseDir}${tribeDirName}/${tribe.code}/${pst.code}/${pst.idle.firstPic}"></img></td>
  <td>
    <table>
    [#list pst.buildCost.buildCosts as cost]
      <tr><td>
      [#list 1..cost.amount as i]
      <a href="Ware types.html#${tribe.wareTypes[cost.code].code}"><img border="0" title="${cost.amount} ${i18n._(tribe.wareTypes[cost.code].name)}" src="${baseDir}${tribeDirName}/${tribe.code}/${cost.code}/menu.png"></img></a>
      [/#list]
      </td></tr>
    [/#list]
    </table>
  </td>
</tr>
[/#list]
</table>
</body>
</html>  