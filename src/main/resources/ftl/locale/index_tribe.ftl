[#ftl]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>${i18nTribe._(tribe.name)}</title>
  <link rel="stylesheet" type="text/css" href="../main.css" />
  <link rel="stylesheet" type="text/css" href="../custom.css" />
</head>
<body>
<table width='100%'>
  <tr>
    <td><img src="${baseDir}pics/wl-ico-128.png" alt="widelandslogo"></img></td>
    <td style="vertical-align:middle; text-align:center"><img src="${baseDir}${tribeDirName}/${tribe.code}/headquarters/headquarters_i_00.png"></img><br/>${i18nTribe._(tribe.name)}</td>
  </tr>
    [#list fmConfig.topics as topic]
      <tr>
        <td><a href="${tribe.code}/${topic}.html">${i18nWl._(topic)}</a></td>
        [#if topic_index == 0] 
          <td style="vertical-align:top" rowspan='${fmConfig.topics?size}'>
          ${i18nTribe._(tribe.description)}<br/>
          ${i18nTribe._(tribe.author)}<br/>
          ${i18nTribe._(tribe.carrier2)}<br/>
          </td>
        [/#if]
      </tr>
    [/#list]
</table>

</body>
</html>  