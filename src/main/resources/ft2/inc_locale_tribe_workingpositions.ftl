[#ftl]
<!-- START inc_locale_tribe_workingpositions.ftl -->
<table class="workingpositions">
[#if subConf.getSection("working positions")?? ]
[#assign workingPositions = subConf.getSection("working positions")]
[#list workingPositions.keys as workerTypeCode]
<tr><td>
  [#list 1..workingPositions.getSingleInt(workerTypeCode) as i]
  <a href="worker types.html#${workerTypeCode}"><img border="0" title="${workingPositions.getSingleInt(workerTypeCode)} ${LC._(localeCode,'tribe_'+tribeCode, conf.getTribe(tribeCode).get("worker types", workerTypeCode))}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${workerTypeCode}/menu.png"></img></a>
  [/#list]
</td></tr>  
[/#list]
[#else]
<tr><td><!-- no build cost given--></td></tr>
[/#if]
</table>
<!-- END inc_locale_tribe_workingpositions.ftl -->
	  		