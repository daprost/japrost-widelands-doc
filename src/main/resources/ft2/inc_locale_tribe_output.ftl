[#ftl]
<!-- START inc_locale_tribe_output.ftl -->
<table class="output">
[#if subConf.getList("#default#","output")?? ]
[#assign output = subConf.getList("#default#", "output")]
[#list output as wareTypeCode]
<tr><td>
  <a href="ware types.html#${wareTypeCode}"><img border="0" title="${LC._(localeCode,'tribe_'+tribeCode, conf.getTribe(tribeCode).get("ware types", wareTypeCode))}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png"></img></a>
</td></tr>  
[/#list]
[#else]
<tr><td><!-- no output given--></td></tr>
[/#if]
</table>
<!-- END inc_locale_tribe_output.ftl -->
	  		