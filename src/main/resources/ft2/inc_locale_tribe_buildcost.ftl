[#ftl]
<!-- START inc_locale_tribe_buildcost.ftl -->
<table class="buildcost">
[#if subConf.getSection("buildcost")?? ]
[#assign buildCost = subConf.getSection("buildcost")]
[#list buildCost.keys as wareTypeCode]
<tr><td>
  [#list 1..buildCost.getSingleInt(wareTypeCode) as i]
  <a href="ware types.html#${wareTypeCode}"><img border="0" title="${buildCost.getSingleInt(wareTypeCode)} ${LC._(localeCode,'tribe_'+tribeCode, conf.getTribe(tribeCode).get("ware types", wareTypeCode))}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png"></img></a>
  [/#list]
</td></tr>  
[/#list]
[#else]
<tr><td><!-- no build cost given--></td></tr>
[/#if]
</table>
<!-- END inc_locale_tribe_buildcost.ftl -->
	  		