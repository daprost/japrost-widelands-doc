[#ftl]
  <table class="topics">
    [#list topics["topics"] as topic]
      <tr>
        <td>
          <a href="${topic}.html">
          ${LC._(localeCode,"custom","_"+topic)}
          </a>
        </td>
      </tr>
    [/#list]
  </table>
