[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,'widelands','_Buildings')}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>
    
<table class="content">
[#assign pst = conf.getTribe(tribeCode).getSection("productionsite types")]
[#list pst.keys as buildingCode]
  [#assign pstSubConf = conf.getTribe(tribeCode).getSubConf("productionsite types", buildingCode)]
  <tr id="${buildingCode}">
    <th>
      ${LC._(localeCode,'tribe_'+tribeCode,pst.getSingleValue(buildingCode))}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Build cost')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Workers')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Inputs')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Output')}
    </th>
  </tr>
  <tr>
    <td><a href="productionsite types_${buildingCode}.html">
      <img border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${buildingCode}/${pstSubConf.firstIdlePic}"></img>
      </a>
    </td>
    <td>
[#assign subConf = pstSubConf]
[#include "inc_locale_tribe_buildcost.ftl"]
    </td>
    <td>
[#assign subConf = pstSubConf]
[#include "inc_locale_tribe_workingpositions.ftl"]
    </td>
    <td>
[#assign subConf = pstSubConf]
[#include "inc_locale_tribe_inputs.ftl"]
    </td>
    <td>
[#assign subConf = pstSubConf]
[#include "inc_locale_tribe_output.ftl"]
    </td>
  </tr>
  [/#list]
</table>

    </td>
  </tr>
</table>
</body>
</html>  