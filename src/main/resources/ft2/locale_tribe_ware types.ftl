[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,'widelands','_Ware types')}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>

<table class="content">
  <tr>
    <th>
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Type: ')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Description:')}
    </th>
  </tr>
  [#list conf.getTribe(tribeCode).getSection("ware types").keys as typeCode]
  [#assign typeSubConf = conf.getTribe(tribeCode).getSubConf("ware types", typeCode)]
  
  <tr id="${typeCode}">
    <td>
      <a href="ware types_${typeCode}.html"><img border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${typeCode}/menu.png"></img></a>
    </td>
    <td>
      ${LC._(localeCode,"tribe_"+tribeCode,conf.getTribe(tribeCode).get("ware types", typeCode))}
    </td>
    <td>${LC._(localeCode,"tribe_"+tribeCode,typeSubConf.get("#default#", "help"))}</td>
  </tr>
  [/#list]
</table>

    </td>
  </tr>
</table>
</body>
</html>  