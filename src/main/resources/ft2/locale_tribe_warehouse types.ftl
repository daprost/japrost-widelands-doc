[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,tribeCode,'_Warehouse')}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>
    
<table class="content">
[#assign type = conf.getTribe(tribeCode).getSection("warehouse types")]
[#list type.keys as typeCode]
  [#assign typeSubConf = conf.getTribe(tribeCode).getSubConf("warehouse types", typeCode)]
  <tr id="${typeCode}">
    <th>
      ${LC._(localeCode,'tribe_'+tribeCode,type.getSingleValue(typeCode))}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Build cost')}
    </th>
  </tr>
  <tr>
    <td>
      <img src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${typeCode}/${typeSubConf.firstIdlePic}" title="${typeCode}"></img>
    </td>
    <td>
[#assign subConf = typeSubConf]
[#include "inc_locale_tribe_buildcost.ftl"]
    </td>
  </tr>
  [/#list]
</table>

    </td>
  </tr>
</table>
</body>
</html>  