[#ftl]
[#assign head_title = "Widelands (${localeCode})"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_top.ftl"]
<table>
  <tr>
      <td><!-- The Tribes --></td> 
      [#list conf.tribeCodes as tribeCode]
        <td>
          <a href="${tribeCode}/index.html">
            ${LC._(localeCode,"tribe_"+tribeCode,conf.getTribe(tribeCode).get("tribe", "name"))}
            <img border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/headquarters/headquarters_i_00.png"></img>
          </a>
        </td>
      [/#list]
    </tr>
</table>

</body>
</html>  