[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]

<table width='100%'>
  <tr>
    <td>
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>
      ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'descr'))}
      <br/>
      ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'author'))}
    </td>
  </tr>
</table>

</body>
</html>  