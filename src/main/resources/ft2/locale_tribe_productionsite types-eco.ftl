[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,'widelands','_Buildings')} ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).getSection('productionsite types').getSingleValue(typeCode))}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
		<td>
<table class="economy">
	<tbody>
		<tr>
			<td>
			<table>
				<tbody>
[#if eco.getTribeEconomy(tribeCode).getConsumptionWareTypes(typeCode)?size == 1]
  [#list eco.getTribeEconomy(tribeCode).getConsumptionWareTypes(typeCode) as ecoEntry]
    [#assign wareTypeCode = ecoEntry.typeCode]
    [#assign sectionCode = ecoEntry.sectionCode]
					<tr>
						<td>
						  <a href="${sectionCode}_${wareTypeCode}.html">
						    <img class="type" border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png" title="${wareTypeCode}"/>
							  ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('ware types', wareTypeCode))}
							</a>
						</td>
						<td><img height="48" src="../../images/ecoLeftSingle.png" /></td>
					</tr>
  [/#list]
[#else]				
  [#list eco.getTribeEconomy(tribeCode).getConsumptionWareTypes(typeCode) as ecoEntry]
    [#assign wareTypeCode = ecoEntry.typeCode]
    [#assign sectionCode = ecoEntry.sectionCode]
					<tr>
						<td>
						  <a href="${sectionCode}_${wareTypeCode}.html">
						    <img class="type" border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png" title="${wareTypeCode}"/>
							  ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('ware types', wareTypeCode))}
							</a>
						</td>
    [#if ecoEntry_index==0]
						<td><img height="48" src="../../images/ecoLeftUpper.png" /></td>
    [#elseif ecoEntry_has_next]
						<td><img height="48" src="../../images/ecoLeftMid.png" /></td>
    [#else]
						<td><img height="48" src="../../images/ecoLeftLower.png" /></td>
    [/#if]
					</tr>
  [/#list]
[/#if]
				</tbody>
			</table>
			</td>
[#if eco.getTribeEconomy(tribeCode).getConsumptionWareTypes(typeCode)?size > 0]				
			<td><img height="48" 
				src="../../images/ecoMidLeft.png" />
			</td>
[/#if]
			<td>
			  <img class="type" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${typeCode}/${conf.getTribe(tribeCode).confOfBuilding(typeCode).firstIdlePic}" />
			  ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).getSection('productionsite types').getSingleValue(typeCode))}
			 </td>
			<td><img height="48" 
				src="../../images/ecoMidRight.png" />
			</td>
			<td>
			<table>
				<tbody>
[#if eco.getTribeEconomy(tribeCode).getProductionWareTypes(typeCode)?size == 1]
  [#list eco.getTribeEconomy(tribeCode).getProductionWareTypes(typeCode) as ecoEntry]
    [#assign wareTypeCode = ecoEntry.typeCode]
    [#assign sectionCode = ecoEntry.sectionCode]
					<tr>
						<td><img height="48" src="../../images/ecoRightSingle.png"/></td>
						<td>
						  <a href="${sectionCode}_${wareTypeCode}.html">
						    <img class="type" border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png" title="${wareTypeCode}"/>
							  ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('ware types', wareTypeCode))}
							</a>
						</td>
					</tr>
  [/#list]
[#else]				
  [#list eco.getTribeEconomy(tribeCode).getProductionWareTypes(typeCode) as ecoEntry]
    [#assign wareTypeCode = ecoEntry.typeCode]
    [#assign sectionCode = ecoEntry.sectionCode]
					<tr>
    [#if ecoEntry_index==0]
						<td><img height="48" src="../../images/ecoRightUpper.png" /></td>
    [#elseif ecoEntry_has_next]
						<td><img height="48" src="../../images/ecoRightMid.png" /></td>
    [#else]
						<td><img height="48" src="../../images/ecoRightLower.png" /></td>
    [/#if]
						<td>
						  <a href="${sectionCode}_${wareTypeCode}.html">
						    <img class="type" border="0" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${wareTypeCode}/menu.png" title="${wareTypeCode}"/>
							  ${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('ware types', wareTypeCode))}
							</a>
						</td>
					</tr>
  [/#list]
[/#if]
				</tbody>
			</table>
			</td>
			
		</tr>
	</tbody>
</table>
    </td>
  </tr>
</table>
</body>
</html>  