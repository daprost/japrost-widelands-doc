[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,'widelands','_Worker types')}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>

<table class="content">
  <tr>
    <th>
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Type: ')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Description:')}
    </th>
    <th>
      ${LC._(localeCode,'widelands','_Build cost')}
    </th>
  </tr>
  [#assign wts = conf.getTribe(tribeCode).getSection("worker types")]
  [#list wts.keys as workerTypeCode]
  [#assign workerTypeSubConf = conf.getTribe(tribeCode).getSubConf("worker types", workerTypeCode)]
  <tr id="${workerTypeCode}">
    <td>
      <img src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${workerTypeCode}/menu.png"></img>
    </td>
    <td>
      ${LC._(localeCode,"tribe_"+tribeCode,wts.getSingleValue( workerTypeCode))}
    </td>
    <td>
      ${LC._(localeCode,"tribe_"+tribeCode,workerTypeSubConf.get("#default#", "help"))}
    </td>
    <td>
[#assign subConf = workerTypeSubConf]
[#include "inc_locale_tribe_buildcost.ftl"]
    </td>
  </tr>
  [/#list]
</table>

    </td>
  </tr>
</table>
</body>
</html>  