[#ftl]

[#macro displaySection conf sectionCode]
  [#local confSection=conf.getSection(sectionCode)]
  <table border="1">
    [#list confSection.keys as key]
    <tr>
      [#if confSection.isValueList(key)]
      <td>${key} </td><td>
        [#list confSection.getValueList(key) as value]
        ${value} (${LC._("de","no",value)}),
        [/#list]
      </td>
      [#else]
      <td>${key} </td><td>${confSection.getSingleValue(key)} (${LC._("de","no",confSection.getSingleValue(key))})</td>
      [/#if]
     </tr>
     [/#list]
   </table>
[/#macro]

[#macro displaySectionWithSub conf sectionCode]
  [#local confSection=conf.getSection(sectionCode)]
  [#-- ConfSection "Worker Types" --]
  <table border="1">
    [#list confSection.keys as key]
    [#-- key = Worker Type Code --]
    <tr>
      <td>${key}</td><td>${confSection.getSingleValue(key)}  (${LC._("de","no",confSection.getSingleValue(key))})
        [#if conf.getSubConf(sectionCode, key)??]
          [#local subConf=conf.getSubConf(sectionCode, key)]
          [#if subConf.size > 0]
          <table border="1">
            [#list subConf.sectionCodes as subSectionCode]
            <tr>
              <td>${subSectionCode}[@displaySection conf=subConf sectionCode=subSectionCode/]</td>
            </tr>
            [/#list]
          </table>
          [/#if]
        [/#if]
      </td>
     </tr>
     [/#list]
   </table>
[/#macro]

<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
  <title>Widelands Fact Sheet</title>
  <link rel="stylesheet" type="text/css" href="main.css" />
  <link rel="stylesheet" type="text/css" href="custom.css" />
</head>
<body>
<table border="1">
  <tr>
    <td>All the Tribes</td>
  </tr>
 [#list conf.tribeCodes as tribeCode]
  <tr>
    <td>${tribeCode}
      <table border="1">
        [#assign tribeConf = conf.getTribe(tribeCode)]
        [#list tribeConf.sectionCodes as tribeSectionCode]
        <tr>
          <td>${tribeSectionCode}[@displaySectionWithSub conf=tribeConf sectionCode=tribeSectionCode/]</td>
        </tr>
        [/#list]
        
      </table>
    </td>
  </tr>
  [/#list]
</table>

</body>
</html>  