[#ftl]
[#assign head_title = "Widelands Fact Sheet"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
<table>
  <tr>
    <td><img src="${baseDir}pics/wl-ico-128.png" alt="widelandslogo"></img></td>
    <td colspan='${conf.tribeCodes?size}' style="vertical-align:middle; text-align:center">Widelands Facts</td>
  </tr>
  <tr>
      <td>select locale</td> 
  </tr>
  [#list LC.localeCodes as localeCode]
    <tr>
      <td>
      <a href="${localeCode}/index.html">${localeCode}</a></td>
      </td>
    </tr>
  [/#list]
  
</table>

</body>
</html>  