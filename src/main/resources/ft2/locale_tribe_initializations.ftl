[#ftl]
[#assign head_title = "${LC._(localeCode,'tribe_'+tribeCode,conf.getTribe(tribeCode).get('tribe', 'name'))} ${LC._(localeCode,'widelands','_Trainingsite')}"]
<?xml version="1.0" encoding="UTF-8" ?>
<html>
<head>
[#include "inc_ALL_head.ftl"]
</head>
<body>
[#include "inc_locale_tribe_top.ftl"]
<table class="body">
  <tr>
    <td style="vertical-align:top; text-align:center">
      [#include "inc_locale_tribe_topics.ftl"]
    </td>
    <td>
    
<table class="content">
[#assign type = conf.getTribe(tribeCode).getSection("initializations")]
[#list type.keys as typeCode]
  [#assign typeSubConf = conf.getTribe(tribeCode).getSubConf("initializations", typeCode)]
  <tr>
    <th>
      ${LC._(localeCode,'tribe_'+tribeCode,type.getSingleValue(typeCode))}
    </th>
  </tr>
  <tr id="${typeCode}">
    <td>
      <table class="initializations">
        <tr>
          <td>
            ${LC._(localeCode,'widelands','_Workers')}
          </td>
          <td>
          <!-- display allowed worker types -->
          [#list typeSubConf.getSection("allow_worker_types").getValueList("worker") as workerTypeCode]
              <a href="Worker types.html#${workerTypeCode}"><img border="0" title="${LC._(localeCode,'tribe_'+tribeCode, conf.getTribe(tribeCode).get("worker types", workerTypeCode))}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${workerTypeCode}/menu.png"></img></a>
          [/#list]
          </td> 
        </tr>
        <tr>
          <td>
            ${LC._(localeCode,'widelands','_Buildings')}
          </td>
          <td>
      		<!-- display buildings (later with contents)-->
      		[#list typeSubConf.sectionCodes as sectionCode]
      		
      		[#if conf.getTribe(tribeCode).getSubConf("productionsite types", sectionCode)??]
      		  [#assign pstSubConf = conf.getTribe(tribeCode).getSubConf("productionsite types", sectionCode)]
      		    <a href="Buildings.html#${sectionCode}"><img border="0" title="${sectionCode}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${sectionCode}/${pstSubConf.firstIdlePic}"></img></a>  
      		[#elseif conf.getTribe(tribeCode).getSubConf("militarysite types", sectionCode)??]
      		  [#assign pstSubConf = conf.getTribe(tribeCode).getSubConf("militarysite types", sectionCode)]
      		    <a href="Military.html#${sectionCode}"><img border="0" title="${sectionCode}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${sectionCode}/${pstSubConf.firstIdlePic}"></img></a>  
      		[#elseif conf.getTribe(tribeCode).getSubConf("trainingsite types", sectionCode)??]
      		  [#assign pstSubConf = conf.getTribe(tribeCode).getSubConf("trainingsite types", sectionCode)]
      		    <a href="Trainingsite.html#${sectionCode}"><img border="0" title="${sectionCode}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${sectionCode}/${pstSubConf.firstIdlePic}"></img></a>  
      		[#elseif conf.getTribe(tribeCode).getSubConf("warehouse types", sectionCode)??]
      		  [#assign pstSubConf = conf.getTribe(tribeCode).getSubConf("warehouse types", sectionCode)]
      		    <a href="Warehouse.html#${sectionCode}"><img border="0" title="${sectionCode}" src="${baseDir}${conf.tribeConfDirName}/${tribeCode}/${sectionCode}/${pstSubConf.firstIdlePic}"></img></a>  
      		[/#if]
      		[/#list]
      		</td>
      	</tr>	
      </table>
    </td>
  </tr>
  [/#list]
</table>

    </td>
  </tr>
</table>
</body>
</html>  